import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    filename = '/tmp/ansible-vars'

    assert(host.file(filename).exists)
    assert(host.file(filename).is_file)

    # Check if the headers of the template are there.
    assert(host.file(filename).contains('Environment Variables ("environment"):'))
    assert(host.file(filename).contains('GROUP NAMES Variables ("group_names"):'))
    assert(host.file(filename).contains('GROUPS Variables ("groups"):'))
    assert(host.file(filename).contains('HOST Variables for this host '))

    # Check if important ansible variables can be found.
    assert(host.file(filename).contains('"ansible_architecture":'))
    assert(host.file(filename).contains('"ansible_date_time": {'))
    assert(host.file(filename).contains('"ansible_version": {'))
