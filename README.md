# Ansible Role: dumpvars

Create a file on the target system containing the relevant Ansible variables available to that host. Useful for debugging.

This role is not idempotent; it will overwrite the file if it already exists. To mitigate this behavior, you should tag this role with `never` and `debug`.

## Requirements

None.

## Role variables

| Variable names      | Default values      | Description |
| :------------------ | :------------------ | :---------- |
| `dumpvars_filepath` | `/tmp/ansible-vars` | Allows to set a different filepath where the Ansible vars will be dumped. |

## Dependencies

None.

## Examples

### Requirements

    - name: wolfmah.dumpvars
      src: git@gitlab.com:home.lan/infra-tools/ansible-roles/dumpvars.git
      scm: git
      version: master

### Playbook

    - hosts: servers
      roles:
        - role: wolfmah.dumpvars
          vars:
            - dumpvars_filepath: /tmp/ansible-vars
          tags:
            - never
            - debug

## Ownership and License

This project uses [C4 (Collective Code Construction Contract)](https://rfc.harbec.site/spec-2/C4/) process for contributions.

This project uses the Mozilla Public License Version 2.0 (MPLv2) license, see [LICENSE](LICENSE).

The contributors are listed in [AUTHORS](AUTHORS).

To report an issue, use the GitLab project [issue tracker](https://gitlab.com/home.lan/infra-tools/ansible-roles/dumpvars/issues).
